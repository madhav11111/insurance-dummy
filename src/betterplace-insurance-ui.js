import React from 'react';
import 'react/jsx-runtime';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import Root from './index';

const lifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Root,
  errorBoundary(err, info, props) { // eslint-disable-line no-unused-vars
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});

export const { bootstrap, mount, unmount } = lifecycles;
