const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const Dotenv = require('dotenv-webpack');
// const getClientEnvironment = require('./env');
const path = require('path');
const fs = require('fs');

module.exports = (webpackConfigEnv, argv) => {
  const dotnavPath = `.env.${process.env.ENV || 'local'}`;

  const currentDirectory = fs.realpathSync(process.cwd());
  const env = new Dotenv({
    path: path.resolve(currentDirectory, dotnavPath),
  });

  const defaultConfig = singleSpaDefaults({
    orgName: 'betterplace',
    projectName: 'insurance-ui',
    webpackConfigEnv,
    argv,
  });

  // defaultConfig.entry = "./src/custom/index.js";
  defaultConfig.devServer = {
    port: 8090,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    proxy: {
      '/api': {
        target: 'https://api-dev.betterplace.co.in/',
        changeOrigin: true,
      },
    },
  };
  defaultConfig.module.rules = [
    {
      test: /\.js\.map$/,
      enforce: 'pre',
      use: [
        {
          loader: 'file-loader',
          options: {
            esModule: false,
          },
        },
      ],

    }, {
      test: /\.(js|jsx)$/,
      use: ['babel-loader'],
    },
    {
      test: /\.css$/i,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: true,
            esModule: false,
          },
        },
      ],
    },
    {
      test: /\.(png|jpg|gif|svg)$/i,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8192,
            esModule: false,
          },
        },
      ],
    },
    {
      test: /\.module\.scss$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: {
              localIdentName: '[name]__[local]',
            },
          },
        },
        'sass-loader',
      ],
    },
    {
      test: /.scss$/,
      exclude: /\.module\.scss$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: {
              compileType: 'icss',
            },
          },
        },
        'sass-loader',
      ],
    },
    {
      test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          },
        },
      ],
    },

  ];

  const config = merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    devtool: 'source-map',
    plugins: [
      env,
    ],
    target: ['web', 'es5'],
    resolve: {
      fallback: {
        crypto: false,
        'crypto-browserify': require.resolve('crypto-browserify'),
      },
    },
  });

  // throw 0;
  return config;
};
